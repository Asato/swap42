# swap42

	swap42
	a tool for Linux* to map host directories to a raw hdd image file
	meant for working with PC Emulators (like PCem, QEMU) to easily 
	copy files from host to guest or for cross platform development (MSDOS)

	Copyright (C) 2021 Asato/Unchained (unchainedgroup.wordpress.com) 
	Developed on VOID Linux with GCC 10.2.1

	GPL3 licensed
  
	swap42
	* creates an raw hdd image defined by CHS values (16 MB to 2GB)
	* with one primary partition & FAT16 file system for MSDOS
	* copies content of a host dir into it
	* allows rescaning the host from inside the guest when contents of 
	  host dir changed (special rescan.com for MSDOS 5.0)
	* maps the image file to memory to monitor rescan cmd 
	
	Notes
	* host files and directories are simply skipped when there is not 
	  enough disk space left on image or they are not accessible
	* the image file is created or overwritten
	* for Windows 98 set disk in hw manager to "removable", 
	  Windows then detects filesystem changes, invoke rescan by HOST with 'R'
	* under MSDOS you MUST use rescan.com, don't invoke by HOST!

	Tested with
	* HOST OS: Linux (Void, Debian), (*) WSL (Windows Subsystem for Linux)
	  (WSL: the image file is locked, so refreshing from HOST/Guest is not possible)
	* Guest OS: MSDOS 5.0, Windows 98
	* Emulators: PCem v17, QEMU 5.2, MAME 0.229, Bochs 2.6.11
	
	Updates	
	* 27.04.	- key to invoke rescan by host application