; RESCAN Tool for swap42
; Copyright (C) 2021 Asato/Unchained (unchainedgroup.wordpress.com)
; use FASM to compile

org 0x100
use16
	jmp main

	drive_number 	db 2				; start drive number 2 (C:)
	not_found		db 'swap42 drive not found!', 10, 13, '$'
	false_ver		db 'MS DOS 4.0+ required!', 10, 13, '$'
	no_success		db 'Rescan canceled!', 10, 13, '$'

	AbsDiskIORec 	dd 0				; starting logical sector	(bootsector, dos 4.0+)
					dw 1				; count of sectors
					dw vbr				; far pointer data buffer, ofs
					dw 0				; seg

	STATUS_FLAG_POSITION = 0x25 	  	; Position of status flag in VBR
	RESCAN_CMD = 0xBB 				  	; set by GUEST: guest wants rescan
	RESCAN_SUCCESS = 0xAA			  	; set by HOST: id flag & rescan done				

error:
	mov ah, 9
	int 0x21
	
	mov ah, 0x4C
	int 0x21

check_ver:
	mov ah, 0x30
	mov al, 0
	int 0x21
	cmp al, 4
	jae .okay
	
	mov dx, false_ver
	call error

.okay:
	retn

flush_disk_buffers:
	mov ah, 0xD
	int 0x21
	retn

read_vbr:
	call flush_disk_buffers

	mov al, [drive_number]		
	mov cx, 0xFFFF	
	mov dx, 0							; starting sector - bootsector
	mov bx, AbsDiskIORec
	int 0x25							; CF = 0 - no error, else ax error
	pop cx								; int 25h leaves a word on stack

	retn

write_vbr:
	call flush_disk_buffers

	mov al, [drive_number]
	mov cx, 0xFFFF
	mov dx, 0
	mov bx, AbsDiskIORec
	int 0x26
	pop cx
	
	retn

find_host_drive:
	call read_vbr
	
	mov bx, vbr
	cmp [bx + STATUS_FLAG_POSITION], byte RESCAN_SUCCESS
	je .found_host

	inc [drive_number]
	cmp [drive_number], 26		; Z?
	jb find_host_drive
	
	mov dx, not_found
	call error

.found_host:
	retn

main:
	push ds
	pop dx

	mov bx, AbsDiskIORec
	mov [bx + 8], dx						; ds for function read vbr
	
	call find_host_drive					; is there a mounted swap42 drive?

	mov bx, vbr
	mov [bx + STATUS_FLAG_POSITION], byte RESCAN_CMD
	
	call write_vbr

.wait_finish:

	call read_vbr
	
	mov bx, vbr
	cmp [bx + STATUS_FLAG_POSITION], byte RESCAN_SUCCESS
	je .rescan_finished

	mov ah, 6
	mov dl, 0xff
	int 0x21
	jz .wait_finish							 ; cancel at keypress

	mov dx, no_success
	call error
	
.rescan_finished:	
	mov ah, 0x4C
	int 0x21
	
	vbr rb (512)
