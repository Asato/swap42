/*
	swap42
	a tool for Linux to map host directories to a raw hdd image file
	meant for working with PC Emulators (like PCem, QEMU) to easily
	copy files from host to guest or for cross platform development (MSDOS)

	Copyright (C) 2021 Asato/Unchained (unchainedgroup.wordpress.com)
	Developed on VOID Linux with GCC 10.2.1

	GPL3 licensed

	swap42
	* creates an raw hdd image defined by CHS values (16 MB to 2GB)
	* with one primary partition & FAT16 file system for MSDOS
	* copies content of a host dir into it
	* allows rescaning the host from inside the guest when contents of
	  host dir changed (special rescan.com for MSDOS 5.0)
	* maps the image file to memory to monitor rescan cmd

	Notes
	* host files and directories are simply skipped when there is not
	  enough disk space left on image or they are not accessible
	* the image file is created or overwritten
	* for Windows 98 set disk in hw manager to removable 

	Tested with
	* Guest OS: MSDOS 5.0, Windows 98
	* Emulators: PCem v17, QEMU 5.2
	
	Updates	
	* 27.04.	- key to invoke rescan by host application
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/statfs.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>
#include <poll.h>

#define MIN_SIZE_PARTITION 16777216 				// FAT16 used for HDD > 16 MB
#define MAX_SIZE_PARTITION 2147483648				// FAT16 max 2GB partition
#define STATUS_FLAG_POSITION 0x25					// Position of status flag in VBR
#define RESCAN_CMD 0xBB								// set by GUEST: guest wants rescan host dir
#define EXPORT_CMD 0xCC								// set by GUEST: guest wants to export files from guest to host dir (not implemented)
#define UNMOUNT_CMD 0xFF							// set by GUEST: guest unmounts
#define RESCAN_SUCCESS 0xAA							// set by HOST: rescan done				
#define PARTITION_TABLE_OFFSET 446					// start of partition table
#define VBR_OFFSET 11								// start of struct vbr
#define VOLUME_SERIAL_NUMBER_OFS 0x27

// basic MBR (512b)

uint8_t basic_mbr[512] = {
    [0 ... 509] = 0,
    0x55, 0xAA
};

struct __attribute__ ((__packed__)) partition_table {
    uint8_t bootable;				// 0: not
    uint8_t first_sector_1;			// CHS first sector
    uint8_t first_sector_2;
    uint8_t first_sector_3;
    uint8_t partition_type;
    uint8_t last_sector_1; 			// CHS last sector
    uint8_t last_sector_2;
    uint8_t last_sector_3;
    uint32_t LBA_starting_sector;	// 0: MBR
    uint32_t number_of_sectors;
} swap42_partition = {};

// basic VBR (512b) (DOS bootsector or BPB)

uint8_t basic_vbr[512] = {
    0xEB, 0xFE, 0x90,									// jmp code
    'M', 'S', 'D', 'O', 'S', '5', '.', '0',				// OEM - necessary to force MS DOS to read VBR
    [11 ... 42] = 0,									// see struct vbr
    'H', 'O', 'S', 'T', ' ', ' ',' ',' ',' ',' ',' ',	// volume label
    'F', 'A', 'T', '1', '6', ' ', ' ', ' ',				// guess ...
    [62 ... 509] = 0,
    0x55, 0xAA
};

struct __attribute__ ((__packed__)) vbr {
    uint16_t bytes_per_sector;
    uint8_t	sectors_per_cluster;
    uint16_t reserved_sectors;
    uint8_t number_of_fats;
    uint16_t max_number_of_root_entries;
    uint16_t notset;
    uint8_t media_descriptor;
    uint16_t sectors_per_fat;
    uint16_t sectors_per_track;
    uint16_t heads;
    uint32_t hidden_sectors;
    uint32_t number_of_sectors;
    uint8_t	drive_number;
    uint8_t status_flag;
    uint8_t signature_byte;
    uint32_t volume_serial_number;
} swap42_vbr = {};

// empty FAT
uint8_t empty_fat16[] = { 	0xF8, 0xFF,		// cluster 0: FAT ID
                            0xFF, 0xFF
                        };	// cluster 1: end of cluster chain marker - clean drive
							// first data cluster is 2 after root directory table

struct __attribute__ ((__packed__)) fat16_directory_entry {
    uint8_t filename[8];
    uint8_t ext[3];
    uint8_t file_attr;						// 0x10: Directory
    uint8_t dont_care[10];
    uint16_t mod_time;						// bits 15-11: hour, 10-5: minutes, 4-0: seconds/2
    uint16_t mod_date;						// bits 15-9: year (1980=0), 8-5 months, 4-0: day
    uint16_t starting_cluster;
    uint32_t filesize;
};

struct file_conv_table {
    int same_entries;
    char fat16_filename[9];
    char fat16_ext[4];
    struct file_conv_table *next_table_entry;
};

struct image_properties {
    long size_of_image;
    int free_disk_space;
    int avail_disk_space;
    int cluster_size;
    int num_of_FAT_entries;
    int start_of_vbr;
    int start_of_fat;
    int start_of_root;
    int start_of_data;
} swap42_disk = {};

int calculate_disk_essentials(int cylinders, int heads, int sectors)
{
    int tmp;

    if (cylinders > 1023 || heads > 255 || sectors > 63) return 1;

    swap42_disk.size_of_image = cylinders * heads * sectors; // total sectors

    // partition table setting

    swap42_partition.partition_type = 6; 					// primary

    swap42_partition.first_sector_1 = 1; 					// CHS Values
    swap42_partition.first_sector_2 = 1;
    swap42_partition.first_sector_3 = 0;

    swap42_partition.last_sector_1 = heads - 1;				// CHS Values
    swap42_partition.last_sector_2 = sectors + (((cylinders - 1) >> 2) & 0xC0);
    swap42_partition.last_sector_3 = (cylinders - 1) & 0xFF;

    swap42_partition.LBA_starting_sector = sectors;
    swap42_partition.number_of_sectors = swap42_disk.size_of_image - sectors;

    // basic VBR settings
    swap42_vbr.bytes_per_sector = 512;
    swap42_vbr.drive_number = 0x80;
    swap42_vbr.heads = heads;
    swap42_vbr.hidden_sectors = sectors;
    swap42_vbr.max_number_of_root_entries = 512;	// FAT16 vanilla style
    swap42_vbr.number_of_fats = 2;					// FAT16 vanilla style
    swap42_vbr.sectors_per_track = sectors;
    swap42_vbr.media_descriptor = 0xF8;				// hdd
    swap42_vbr.reserved_sectors = 1;				// just VBR
    swap42_vbr.signature_byte = 0x29;
    swap42_vbr.number_of_sectors = swap42_partition.number_of_sectors;

    // size of partition too small?
    if (swap42_vbr.number_of_sectors * swap42_vbr.bytes_per_sector < MIN_SIZE_PARTITION) return 2;
    if (swap42_vbr.number_of_sectors * swap42_vbr.bytes_per_sector > MAX_SIZE_PARTITION) return 3;

    // and some more ...

    swap42_disk.size_of_image = swap42_disk.size_of_image * 512; // in bytes
    swap42_disk.start_of_vbr = swap42_vbr.hidden_sectors * swap42_vbr.bytes_per_sector;
    swap42_disk.start_of_fat = swap42_disk.start_of_vbr + swap42_vbr.reserved_sectors * swap42_vbr.bytes_per_sector;

    // calculate clustersize for 65533 possible Entries in FAT
    tmp = 0;
    while ((1 << tmp) * swap42_vbr.bytes_per_sector < (swap42_disk.size_of_image / 0xFFFD)) tmp++;
    swap42_vbr.sectors_per_cluster = 1 << tmp;
    swap42_disk.cluster_size = swap42_vbr.sectors_per_cluster * swap42_vbr.bytes_per_sector;

    // size of FAT (sectors) - how many clusters are needed (2 byte)
    swap42_vbr.sectors_per_fat = (((swap42_disk.size_of_image / swap42_disk.cluster_size) * 2) / swap42_vbr.bytes_per_sector) + 1;
    swap42_disk.start_of_root = swap42_disk.start_of_fat + swap42_vbr.number_of_fats * swap42_vbr.sectors_per_fat * swap42_vbr.bytes_per_sector;

    // after root directory table, each entry 32 Bytes, align to sectors
    tmp = (swap42_vbr.max_number_of_root_entries * 32) % swap42_vbr.bytes_per_sector;
    if (tmp != 0) tmp = 1;
    swap42_disk.start_of_data = swap42_disk.start_of_root + ((swap42_vbr.max_number_of_root_entries * 32 / swap42_vbr.bytes_per_sector) + tmp) * swap42_vbr.bytes_per_sector;

    // the rest is free disk space - num of sectors * bytes per sector - start of data, but reduced to a multiple of full clusters
    swap42_disk.avail_disk_space = (((swap42_vbr.number_of_sectors * swap42_vbr.bytes_per_sector) - swap42_disk.start_of_data) / swap42_disk.cluster_size) * swap42_disk.cluster_size;

    // write to MBR & VBR
    memcpy(basic_mbr + PARTITION_TABLE_OFFSET, &swap42_partition, sizeof(swap42_partition));
    memcpy(basic_vbr + VBR_OFFSET, &swap42_vbr, sizeof(swap42_vbr));
    return 0;
}

int open_create_image_file(char *filename)
{
    off_t filesize;
    int fd_dev_zero, fd_image;
    struct statfs disk_properties;
    long diskspace;

    // try to open/create image (O_DIRECT undeclared?)
    fd_image = open(filename, O_RDWR|O_CREAT|O_APPEND|O_SYNC,  S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP);
    if (fd_image == -1) return -1;

    // enough space on disk?
    if (fstatfs(fd_image, &disk_properties) == -1) return -5;
    diskspace = disk_properties.f_bavail * disk_properties.f_bsize;

    if (diskspace < swap42_disk.size_of_image) return -6;

    // test filesize
    filesize = lseek(fd_image, 0, SEEK_END);
    if (filesize == -1) return -2;

    // file too small? expand
    if (filesize < swap42_disk.size_of_image && fallocate(fd_image, 0, filesize, swap42_disk.size_of_image - filesize) == -1) return -3;

    // file too big? shrink
    if (filesize > swap42_disk.size_of_image && ftruncate(fd_image, swap42_disk.size_of_image) == -1) return -4;

    return fd_image;
}

uint8_t *map_image_to_memory(int fd_image)
{
    return (uint8_t *) mmap(NULL, swap42_disk.size_of_image, PROT_READ|PROT_WRITE, MAP_SHARED, fd_image, 0);
}

void initialize_image(uint8_t *image)
{
    int t;

    memset(image, 0, swap42_disk.size_of_image);
    memcpy(image, basic_mbr, 512);
    memcpy(image + swap42_disk.start_of_vbr, basic_vbr, 512);
    for (t = 0; t < swap42_vbr.number_of_fats; t++) {
        memcpy(image + swap42_disk.start_of_fat + swap42_vbr.sectors_per_fat * swap42_vbr.bytes_per_sector * t, empty_fat16, 4);
    }
    swap42_disk.free_disk_space = swap42_disk.avail_disk_space;
}

void release_image(int fd_image, uint8_t *image)
{
    munmap(image, swap42_disk.size_of_image);
    close(fd_image);
}

uint8_t read_status_flag(uint8_t *image)
{
    return image[swap42_disk.start_of_vbr + STATUS_FLAG_POSITION];
}

void write_status_flag(uint8_t *image, uint8_t status)
{
    image[swap42_disk.start_of_vbr + STATUS_FLAG_POSITION] = status;
}

void write_volume_serial_number(uint8_t *image)
{
    time_t lt;
    struct tm *ht;
    uint32_t low, high;

    lt = time(NULL);
    ht = localtime(&lt);

    // not MSDOS style, but a human readable information DDMM - HHMM
    low = (((ht->tm_hour / 10) * 16 + (ht->tm_hour % 10)) << 8) + (((ht->tm_min / 10) * 16) + (ht->tm_min % 10));
    high = (((ht->tm_mday / 10) * 16 + (ht->tm_mday % 10)) << 8) + ((((ht->tm_mon + 1)/ 10) * 16) + ((ht->tm_mon + 1) % 10));

    *((uint32_t*) (image + swap42_disk.start_of_vbr + VOLUME_SERIAL_NUMBER_OFS)) = (high << 16) + low;
}

uint16_t convert_time(int hour, int minutes, int seconds)
{
    return (hour  << 11) + (minutes << 5) + (seconds/2);
}

uint16_t convert_date(int year, int month, int day)
{
    if (year < 1980) year = 1980;
    return ((year - 1980) << 9) + ((month + 1) << 5) + day;
}

void fix_filename(char *src, char *dest, int start_src, int end_src, int max_length_of_dest)
{
    int t, t1;
    uint8_t *legal_chars = "!#$%&'()-@^_`{}";

    for (t=0; t<max_length_of_dest; t++) {
        if (t<end_src-start_src) {
            dest[t] = src[t+start_src];
            // alphanumeric?
            if (isalnum(dest[t]) == 0) {
                // no, but is it a legal char?
                if (strchr(legal_chars, dest[t]) == NULL) {
                    // no - replace by underscore
                    dest[t] = '_';
                }
            } else dest[t] = toupper(dest[t]);		// to uppercase
        } else dest[t] = ' ';						// rest filled with space
    }
}

void convert_filename(char *src_filename, char *fat16_filename, char *fat16_ext)
{
    int pos_in_src;

    // find (something like) an extension
    if (strrchr(src_filename, '.') != NULL) {
        // absolute position of last .
        pos_in_src = strrchr(src_filename, '.') - src_filename;
        // leading .? Only name without leading .
        if (pos_in_src == 0) {
            fix_filename(src_filename, fat16_filename, 1, strlen(src_filename), 8);
            memset(fat16_ext, ' ', 3);
        } else {
            // name ...
            fix_filename(src_filename, fat16_filename, 0, pos_in_src, 8);
            // .. extension
            fix_filename(src_filename, fat16_ext, pos_in_src + 1, strlen(src_filename), 3);
        }
        // no extension
    } else {
        fix_filename(src_filename, fat16_filename, 0, strlen(src_filename), 8);
        memset(fat16_ext, ' ', 3);
    }
}

int create_directory_table_entry(struct fat16_directory_entry *dir_entry, char *full_name, char *fat16_filename, char *fat16_ext, uint8_t file_attr, int starting_cluster)
{

    struct stat file_information;
    struct tm *file_time;

    memset(dir_entry, 0, sizeof(struct fat16_directory_entry));

    if (stat(full_name, &file_information) == -1) return 0;

    dir_entry->starting_cluster = starting_cluster;

    // is dir?
    if (file_attr == 0x10) dir_entry->filesize = 0;
    else {
        // empty file?
        if (file_information.st_size == 0) dir_entry->starting_cluster = 0;
        dir_entry->filesize = file_information.st_size;
    }

    file_time = localtime(&file_information.st_mtime);
    dir_entry->mod_time = convert_time(file_time->tm_hour, file_time->tm_min, file_time->tm_sec);
    dir_entry->mod_date = convert_date(file_time->tm_year + 1900, file_time->tm_mon, file_time->tm_mday);

    memcpy(dir_entry->filename, fat16_filename, 8);
    memcpy(dir_entry->ext, fat16_ext, 3);

    dir_entry->file_attr = file_attr;

    return 1;
}

void write_to_fat(uint8_t *image, int cluster, uint16_t fat_entry)
{
    int t;
    for (t = 0; t < swap42_vbr.number_of_fats; t++) {
        *(uint16_t *) (image + swap42_disk.start_of_fat + swap42_vbr.sectors_per_fat * swap42_vbr.bytes_per_sector * t + cluster * 2) = fat_entry;
    }
}

int write_subdir(uint8_t *image, int filesize, int starting_cluster)
{
    while (filesize > 0) {
        if (filesize > swap42_disk.cluster_size) {
            filesize = filesize - swap42_disk.cluster_size;
            write_to_fat(image, starting_cluster, starting_cluster + 1);
        } else {
            filesize = 0;
            write_to_fat(image, starting_cluster, 0xFFFF);
        }
        swap42_disk.free_disk_space = swap42_disk.free_disk_space - swap42_disk.cluster_size;
        starting_cluster++;
    }

    // return next cluster for next entry
    return starting_cluster;
}

int write_file_to_image(uint8_t *image, int fd_file, int filesize, int starting_cluster)
{
    // no clusters are allocated for empty files ..
    while (filesize > 0) {
        if (filesize > swap42_disk.cluster_size) {
            read(fd_file, image + swap42_disk.start_of_data + (starting_cluster - 2) * swap42_disk.cluster_size, swap42_disk.cluster_size);
            filesize = filesize - swap42_disk.cluster_size;
            write_to_fat(image, starting_cluster, starting_cluster + 1);
        } else {
            read(fd_file, image + swap42_disk.start_of_data + (starting_cluster - 2) * swap42_disk.cluster_size, filesize);
            filesize = 0;
            write_to_fat(image, starting_cluster, 0xFFFF);
        }
        swap42_disk.free_disk_space = swap42_disk.free_disk_space - swap42_disk.cluster_size;
        starting_cluster++;
    }

    // return next cluster for next entry
    return starting_cluster;
}

void fix_duplicate_entries(struct file_conv_table *first_entry, char *fat16_filename, char *fat16_ext)
{
    struct file_conv_table *entry;
    uint8_t tmp_buff[9] = { 0 };
    int tmp;

    tmp = 0;
    entry = first_entry;
    while (tmp == 0) {
        if (strcmp(entry->fat16_filename, fat16_filename) == 0 && strcmp(entry->fat16_ext, fat16_ext) == 0) {
            sprintf(tmp_buff, "~%d",entry->same_entries++);
            memcpy(fat16_filename + 8 - strlen(tmp_buff), tmp_buff, strlen(tmp_buff));
            tmp = 1;
        } else if (entry->next_table_entry != NULL) entry = entry->next_table_entry;
        else tmp = 2; // end of table
    }

    // no match? create new entry in file conv table
    if (tmp == 2) {
        entry->next_table_entry = malloc(sizeof(struct file_conv_table));
        memset(entry->next_table_entry, 0, sizeof(struct file_conv_table));
        entry->next_table_entry->same_entries = 1;
        memcpy(entry->next_table_entry->fat16_filename, fat16_filename, 8);
        memcpy(entry->next_table_entry->fat16_ext, fat16_ext, 3);
    }
}

void remove_conv_table(struct file_conv_table *first_entry)
{
    struct file_conv_table *entry;

    while (first_entry != NULL) {
        entry = first_entry;
        first_entry = entry->next_table_entry;
        free(entry);
    }
}

int valid_file(char *name)
{
    return !((strcmp(name, ".") == 0) || strcmp(name, "..") == 0);
}

int scan_directory(uint8_t *image, char *dir_name, DIR *fd_dir, int starting_cluster, int dirtable_cluster, int max_entries)
{
    DIR *fd_subdir;
    int fd_file;
    struct dirent *dir_entry;
    int count_entries;

    struct fat16_directory_entry this_file;

    struct file_conv_table *first_entry;
    uint8_t fat16_filename[9] = { 0 };
    uint8_t fat16_ext[4] = { 0 };
    char *full_name;

    int subdir_table;
    int adr_dir_table;
    int subdir_cluster;

    int tmp;

    // root dir? adr_dir_table is absolute address
    // subdir .. because of "." & ".." add 2 * 32
    if (dirtable_cluster == 0) adr_dir_table = swap42_disk.start_of_root;
    else adr_dir_table = swap42_disk.start_of_data + (dirtable_cluster - 2) * swap42_disk.cluster_size + 64;

    rewinddir(fd_dir);

    // prepare LFN conversation to FAT16 8.3
    first_entry = malloc(sizeof(struct file_conv_table));
    memset(first_entry, 0, sizeof(struct file_conv_table));

    // write entries to FAT
    while ((dir_entry = readdir(fd_dir)) && swap42_disk.free_disk_space >= swap42_disk.cluster_size && max_entries > 0) {
        // ignore .. & .
        if (valid_file(dir_entry->d_name)) {
            // absolute name for opening/reading
            full_name = malloc(strlen(dir_name) + strlen(dir_entry->d_name) + 2);
            sprintf(full_name, "%s/%s", dir_name, dir_entry->d_name);
            // LFN to 8.3
            convert_filename(dir_entry->d_name, fat16_filename, fat16_ext);
            // and avoid duplicate 8.3 entries
            fix_duplicate_entries(first_entry, fat16_filename, fat16_ext);
            // is file?
            if (dir_entry->d_type == DT_REG) {
                // open file
                fd_file = open(full_name, O_RDONLY);
                // success?
                if (fd_file != -1) {
                    // prepare FAT directory table entry
                    if (create_directory_table_entry(&this_file, full_name, fat16_filename, fat16_ext, 0, starting_cluster)) {
                        // enough space on disk?
                        if (this_file.filesize <= swap42_disk.free_disk_space) {
                            // write file & FAT entries
                            starting_cluster = write_file_to_image(image, fd_file, this_file.filesize, starting_cluster);
                            // write directory table entry
                            memcpy(image + adr_dir_table, &this_file, sizeof(this_file));
                            adr_dir_table = adr_dir_table + 32;
                            max_entries--;
                        }
                    }
                    close(fd_file);
                }
                // is dir?
            } else if (dir_entry->d_type == DT_DIR) {
                // try to open dir
                fd_subdir = opendir(full_name);
                // success?
                if (fd_subdir != NULL) {
                    // subdir is a special file that holds dir table
                    // size (not part of dir table entry) depends on number of entries
                    count_entries = 0;
                    while (dir_entry = readdir(fd_subdir)) count_entries++;
                    // try to create subdir dir table entry - don't create, if even subdir table is too big
                    if (create_directory_table_entry(&this_file, full_name, fat16_filename, fat16_ext, 0x10, starting_cluster) && count_entries * 32 <= swap42_disk.free_disk_space) {
                        // write subdir table entry
                        memcpy(image + adr_dir_table, &this_file, sizeof(this_file));
                        adr_dir_table = adr_dir_table + 32;
                        max_entries--;

                        // prepare recursive call
                        subdir_table = swap42_disk.start_of_data + (starting_cluster - 2) * swap42_disk.cluster_size;

                        // special dir entries
                        // . points to subdir itself
                        create_directory_table_entry(&this_file, full_name, ".       ", "   ", 0x10, starting_cluster);
                        memcpy(image + subdir_table, &this_file, sizeof(this_file));
                        // .. points to parent
                        create_directory_table_entry(&this_file, full_name, "..      ", "   ", 0x10, dirtable_cluster);
                        memcpy(image + subdir_table + 32, &this_file, sizeof(this_file));
                        // reference to subdir entry
                        subdir_cluster = starting_cluster;
                        starting_cluster = write_subdir(image, count_entries * 32, starting_cluster);
                        // scan subdirectory recursively
                        starting_cluster = scan_directory(image, full_name, fd_subdir, starting_cluster, subdir_cluster, count_entries - 2);
                    }
                    closedir(fd_subdir);
                }
            }

            free(full_name);
        }
    }

    remove_conv_table(first_entry);
    return starting_cluster;
}

void rescan_host(uint8_t *image, char *host_directory, DIR *fd_host_dir)
{
    // reinitialize image
    initialize_image(image);

    scan_directory(image, host_directory, fd_host_dir, 2, 0, swap42_vbr.max_number_of_root_entries);

    // new volume serial number
    write_volume_serial_number(image);

    // tell guest we are ready
    write_status_flag(image, RESCAN_SUCCESS);
}

int main(int argc, char* argv[])
{
    int fd_image;
    DIR *fd_host_dir;
    char *tmp_ptr;
    uint8_t *mapped_memory;
    uint8_t guest_cmd;
    long cylinders, heads, sectors;

    char *host_directory;
    char *image_file_name;

    struct pollfd fds;
    fds.fd = STDIN_FILENO;
    fds.events = POLLIN;
    int poll_events;

    time_t lt;
    struct tm *ht;

    puts("*** swap42 host to raw image mapper ***\n");

    // test for valid command line arguments
    switch (argc) {
    case 3:
        cylinders = 820;
        heads = 6;
        sectors = 17;
        calculate_disk_essentials(820, 6, 17);
        break;
    case 6:
        cylinders = strtol(argv[3], &tmp_ptr, 10);
        heads = strtol(argv[4], &tmp_ptr, 10);
        sectors = strtol(argv[5], &tmp_ptr, 10);
        if (cylinders == 0 || heads == 0 || sectors == 0) {
            puts("Invalid CHS (Cmax 1023, Hmax 255, Smax 63) values!");
            exit(EXIT_FAILURE);
        }
        switch (calculate_disk_essentials(cylinders, heads, sectors)) {
        case 1:
            puts("Invalid CHS (Cmax 1023, Hmax 255, Smax 63) values!");
            exit(EXIT_FAILURE);
        case 2:
            puts("Requested image size is too small for FAT16!");
            exit(EXIT_FAILURE);
        case 3:
            puts("Requested image size is too big!");
            exit(EXIT_FAILURE);
        }
        break;
    default:
        printf("%-10s%s", "Usage:", "swap42 IMAGEFILENAME HOSTDIRECTORY [Cylinders] [Heads] [Sectors]\n");
        printf("%10s%s", "", "(default 42MB ST252 820, 6, 17)\n\n");
        printf("%10s%s", "", "Examples (write to BIOS!)\n");
        printf("%10s%s", "", "Model  ST1111A 538, 10, 36 (99 MB)\n");
        printf("%10s%s", "", "Model  ST2274A 536, 16, 55 (241 MB)\n");
        printf("%10s%s", "", "Model  ST3543A 525, 32, 63 (542 MB)\n");
        printf("%10s%s", "", "Model ST31010A 524, 64, 63 (1 GB)\n");
        printf("%10s%s", "", "Model ST32110A 1023, 64, 63 (2 GB)\n\n");
        exit(EXIT_FAILURE);
    }

    // test host directory
    host_directory = realpath(argv[2], NULL);

    if (host_directory == NULL) {
        puts("Invalid host directory!");
        exit(EXIT_FAILURE);
    }

    fd_host_dir = opendir(host_directory);
    if (fd_host_dir == NULL) {
        puts("Invalid host directory!");
        exit(EXIT_FAILURE);
    }

    // try to open/create image file
    image_file_name = argv[1];

    puts("Creating image ...");

    fd_image = open_create_image_file(image_file_name);
    switch (fd_image) {
    case -1:
        puts("Accessing image file failed!");
        exit(EXIT_FAILURE);
    case -2:
        puts("Reading file size of image failed!");
        close(fd_image);
        exit(EXIT_FAILURE);
    case -3:
        puts("Expanding image file failed!");
        close(fd_image);
        exit(EXIT_FAILURE);
    case -4:
        puts("Shrinking image file failed!");
        close(fd_image);
        exit(EXIT_FAILURE);
    case -5:
        puts("Error accessing disk!");
        close(fd_image);
        exit(EXIT_FAILURE);
    case -6:
        puts("Not enough free disk space!");
        close(fd_image);
        exit(EXIT_FAILURE);
    }

    // try to map file to memory

    puts("Mapping image file to memory ...");
    mapped_memory = map_image_to_memory(fd_image);
    if (mapped_memory == (void *) -1) {
        puts("Mapping image file to memory failed");
        close(fd_image);
        exit(EXIT_FAILURE);
    }

    puts("Writing host directory to image ...");

    rescan_host(mapped_memory, host_directory, fd_host_dir);

    // initialize_image(mapped_memory);

    printf("%s mapped to %s\n", host_directory, image_file_name);
    printf("\nImage properties\n");
    printf("%-20s %20d\n", "Cylinders", cylinders);
    printf("%-20s %20d\n", "Heads", heads);
    printf("%-20s %20d\n", "Sectors", sectors);

    printf("%-20s %20d\n", "Disk size", swap42_disk.size_of_image);
    printf("%-20s %20d\n", "Partition size", swap42_partition.number_of_sectors * swap42_vbr.bytes_per_sector);
    printf("%-20s %20d\n", "Free disk space", swap42_disk.avail_disk_space);
    printf("%-20s %20d\n", "Sectors per FAT", swap42_vbr.sectors_per_fat);
    printf("%-20s %20d\n", "Cluster size", swap42_disk.cluster_size);

    printf("%-20s %#20x\n", "VBR offset", swap42_disk.start_of_vbr);
    printf("%-20s %#20x\n", "FAT offset", swap42_disk.start_of_fat);
    printf("%-20s %#20x\n", "Dir table offset", swap42_disk.start_of_root);
    printf("%-20s %#20x\n", "Data area offset", swap42_disk.start_of_data);


    puts("\nWaiting for RESCAN command by GUEST (enter 'Q' to quit or 'R' to rescan)\n");
    do {
        // input on STDIN?
        poll_events = poll(&fds, 1, 0);
        // yes ...
        if (poll_events) {
            switch (getchar()) {
            case 'R':
                guest_cmd = RESCAN_CMD;
                break;
            case 'Q':
                guest_cmd = UNMOUNT_CMD;
            }
            // flush stdin
            while(getchar() != '\n') { }
        } else guest_cmd = read_status_flag(mapped_memory);

        switch (guest_cmd) {
        case RESCAN_CMD:
            lt = time(NULL);
            ht = localtime(&lt);
            printf("[%02d:%02d.%02d] Rescan requested ... \n", ht->tm_hour, ht->tm_min, ht->tm_sec);
            rescan_host(mapped_memory, host_directory, fd_host_dir);
            lt = time(NULL);
            ht = localtime(&lt);
            printf("[%02d:%02d.%02d] Rescan finished.\n", ht->tm_hour, ht->tm_min, ht->tm_sec);
            break;
        }
    } while (guest_cmd != UNMOUNT_CMD);

    closedir(fd_host_dir);
    release_image(fd_image, mapped_memory);
    return 0;
}
